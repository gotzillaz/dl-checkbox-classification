FROM python:3.12.2

WORKDIR /checkbox_classification
COPY . /checkbox_classification

RUN pip3 install -r requirements_eda.txt

EXPOSE 8888

CMD ["jupyter", "notebook", "--no-browser", "--port", "8888", "--allow-root", "--ip", "0.0.0.0"]