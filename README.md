# Checkbox Classification

## **Introduction**
Checkbox classification is becoming increasingly vital in document processing and optical character recognition (OCR), especially in modern web applications where checkboxes may encompass a variety of symbols beyond traditional tick marks.
To address this challenge effectively, a deep-learning model is required.
The expected classes include (i) checked, (ii) unchecked, and (iii) other variations.

## **Repository description**
This repository mainly uses Pytorch and Lightning for prototyping the deep-learning model (i.e., CNN) to classify the given checkbox images.
The source codes in this repository can be separated into two types as follows.

### **1. Exploratory data analysis (EDA)**
#### Purpose
- analyzing the input data
- exploring the data preprocessing and augmentation
- exploring the existing models
- evaluating model performance
- finalizing the best model for the inference.

#### Related file description
- [exploratory_data_analysis.ipynb](exploratory_data_analysis.ipynb) is the main file the EDA process.
- [/data](data) contains the image data for the classification.
- [/lightning_logs](lightning_logs) contains the training, validating, and testing result of classification models, which can be visualized by using Tensorboard library.
    - This directory in the repository does not include the checkpoints of the models except LeNet.
    - Please download the full logs with checkpoints via [this link](https://drive.google.com/file/d/1BOuwJZveQOQKPx18vvlE9sIpqHd3v0Xy/view?usp=sharing)
- [requirements_eda.txt](requirements_eda.txt) contains a list of required dependencies for the EDA code.
- [eda.Dockerfile](eda.Dockerfile) is a Dockerfile to build the EDA code.

#### How to build

- By Docker
    1. Build the Docker image with the following command:
        ```bash
        docker build -t checkbox_eda . -f eda.Dockerfile
        ```
    2. Start the Docker container with the following command:
        ```bash
        docker run -p 8888:8888 checkbox_eda
        ```

- By Python
    1. Install the required dependencies with the following command:
        ```bash
        pip3 install -r requirements_eda.txt
        ```
    2. Start the Jupeter notebook server with the following command:
        ```bash
        jupyter notebook --no-browswer --port 8888 --allow-root --ip 0.0.0.0
        ```

#### How to use
- After building the code from the previous section, please wait for Jupeter notebook server to be initialized and get the access token from the command-line.
    - Jupyter notebook server will be accessible at http://127.0.0.1:8888/tree?token={your_token}.
- Follow all experiments in [exploratory_data_analysis.ipynb](exploratory_data_analysis.ipynb) via Jupyter notebook.

### **2. Command-line interface for checkbox type prediction**
This part contains inference script that takes an image path as input and print the class of that image.

#### Related file description
- [checkbox_classifier.py](checkbox_classifier.py) is the main file for starting the inference process.
- [/modules](modules) contains the model template and data loader class.
- [/model](model) contains the checkpoint file (i.e., model weights and hyperparameters) of the selected model for the inference process.
- [requirements_inference.txt](requirements_inference.txt) contains a list of required dependencies for the inference codes.
- [inference.Dockerfile](inference.Dockerfile) is a Dockerfile to build the inference codes.

#### How to build
- By Docker
    1. Build the Docker image with the following command:
        ```bash
        docker build -t checkbox_inference . -f inference.Dockerfile
        ```
    2. Start the Docker container with the following command:
        ```bash
        docker run -it checkbox_inference /bin/bash
        ```

- By Python
    - Install the required dependencies with the following command:
        ```bash
        pip3 install -r requirements_inference.txt
        ```

#### How to use

Start the model inference with the following command:
```bash
python checkbox_classifier.py <image_file_path>
```

For example:
```bash
python checkbox_classifier.py ./data/checked/90be849038b3ef0bf0fff8cb7bfe55e1.png
```

Note that if you want to add new images for the inference, please add them inside [/data](data) before building process.
