FROM python:3.12.2

WORKDIR /checkbox_classification
COPY . /checkbox_classification

RUN pip3 install -r requirements_inference.txt

CMD ["bash"]