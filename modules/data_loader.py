import os

import torch
import matplotlib.pyplot as plt
from torch.utils.data import Dataset, DataLoader, random_split
from torchvision import transforms

class CheckBoxDatasetPredict(Dataset):
    def __init__(self, root_dir, target_size=(100, 100), transform=None, img_predict_path=None):
        self.root_dir = root_dir
        self.target_size = target_size
        self.transform = transform if transform else transforms.Compose([
            transforms.ToPILImage(),
            transforms.Resize(self.target_size),
            transforms.ToTensor(),
            transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
        ])
        self.classes = os.listdir(root_dir)
        self.class_to_idx = {cls: i for i, cls in enumerate(self.classes)}
        self.idx_to_class = {i: cls for i, cls in enumerate(self.classes)}
        if img_predict_path:
            self.img_paths = [(img_predict_path, -1)]
        else:
            self.img_paths = []
            for cls in self.classes:
                class_path = os.path.join(root_dir, cls)
                for img_name in os.listdir(class_path):
                    img_path = os.path.join(class_path, img_name)
                    self.img_paths.append((img_path, self.class_to_idx[cls]))
            self.img_paths.sort(key=lambda w: (w[1], w[0]))

    def __len__(self):
        return len(self.img_paths)

    def __getitem__(self, idx):
        img_path, label = self.img_paths[idx]
        img = plt.imread(img_path)
        if img.shape[-1] == 4:
            img = img[..., :3]

        img = self.transform(img)

        return img
