import torch
import torch.nn as nn
import torch.nn.functional as F
import lightning as L
from sklearn.metrics import confusion_matrix, accuracy_score, precision_score, recall_score, f1_score

class LeNet(L.LightningModule):
    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv2d(3, 6, kernel_size=5)
        self.conv2 = nn.Conv2d(6, 16, kernel_size=5)
        self.fc0 = nn.Linear(16 * 22 * 22, 120)
        self.fc1 = nn.Linear(120, 84)
        self.fc2 = nn.Linear(84, 3)

    def forward(self, x):
        x = F.relu(self.conv1(x))
        x = F.max_pool2d(x, kernel_size=2, stride=2)
        x = F.relu(self.conv2(x))
        x = F.max_pool2d(x, kernel_size=2, stride=2)
        x = x.view(-1, 16 * 22 * 22)
        x = F.relu(self.fc0(x))
        x = F.relu(self.fc1(x))
        x = self.fc2(x)
        return x

    def configure_optimizers(self):
        return torch.optim.Adam(self.parameters(), lr=0.001)

    def training_step(self, batch, batch_idx):
        return self.performance_logger(batch, 'train')

    def validation_step(self, batch, batch_idx):
        return self.performance_logger(batch, 'val')

    def test_step(self, batch, batch_idx):
        return self.performance_logger(batch, 'test')

    def predict_step(self, batch, batch_idx, dataloader_idx=0):
        return self(batch)

    def performance_logger(self, batch, phase_name):
        x, y = batch
        y_hat = self(x)

        loss = F.cross_entropy(y_hat, y)
        self.log(f'{phase_name}_loss', loss, on_step=True, on_epoch=True, prog_bar=False, logger=True)

        preds = torch.argmax(y_hat, dim=1)
        acc = accuracy_score(y.cpu(), preds.cpu())
        self.log(f'{phase_name}_acc', acc, on_step=True, on_epoch=True, prog_bar=False, logger=True)

        precision = precision_score(y.cpu(), preds.cpu(), average='macro')
        recall = recall_score(y.cpu(), preds.cpu(), average='macro')
        f1 = f1_score(y.cpu(), preds.cpu(), average='macro')
        self.log(f'{phase_name}_precision', precision, on_step=True, on_epoch=True, prog_bar=False, logger=True)
        self.log(f'{phase_name}_recall', recall, on_step=True, on_epoch=True, prog_bar=False, logger=True)
        self.log(f'{phase_name}_f1', f1, on_step=True, on_epoch=True, prog_bar=False, logger=True)

        return loss
