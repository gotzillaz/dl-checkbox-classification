import sys
import logging

from torchvision import transforms
from torch.utils.data import Dataset, DataLoader
import lightning as L
import torch
import matplotlib.pyplot as plt

from modules.lenet_model import LeNet
from modules.data_loader import CheckBoxDatasetPredict

def check_image_validity(img_path):
    try:
        plt.imread(img_path)
        return True
    except Exception as e:
        return False

if __name__ == '__main__':
    logging.getLogger('lightning').setLevel(logging.ERROR)
    if len(sys.argv) != 2:
        print(f'Invalid input path.')
        print(f'Usage: python {__file__.split('/')[-1]} <image_file_path>')
        exit(1)
    img_path = sys.argv[-1]
    if not check_image_validity(img_path):
        print(f'Failed to load an image from {img_path}.')
        print(f'Usage: python {__file__.split('/')[-1]} <image_file_path>')
        exit(1)
    data_file = CheckBoxDatasetPredict('./data', img_predict_path=img_path, target_size=(100, 100))
    data_loader = DataLoader(data_file)

    model = LeNet.load_from_checkpoint('./model/lenet_model.ckpt')

    trainer = L.Trainer(logger=False)
    print(f'Predicted class of {img_path}: {data_file.idx_to_class[torch.argmax(trainer.predict(model, dataloaders=data_loader)[0]).item()]}')
